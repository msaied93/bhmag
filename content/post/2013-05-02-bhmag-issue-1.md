---
author: msaied93
categories:
- أخبار
date: 2013-05-02T21:38:03Z
guid: http://barmagiyathurra.wordpress.com/?p=8
id: 8
tags:
- Dropbox
- Scribd
- العدد الأول
- روابط تنزيل
title: العدد الأول من مجلة البرمجيات الحرة
featured_image: "uploads/2013/issue1-cover.png"
---

روابط التنزيل:

<p dir="RTL" align="RIGHT">
  <a href="https://www.dropbox.com/s/xs2ade6xry1yl8p/BH_magazine1.pdf?m">https://www.dropbox.com/s/xs2ade6xry1yl8p/BH_magazine1.pdf?m</a>
</p>

<p dir="RTL" align="RIGHT">
  رابط Scribd
</p>

<p dir="RTL" align="RIGHT">
  <a href="http://bit.ly/14cAT0A">http://bit.ly/14cAT0A</a>
</p>
