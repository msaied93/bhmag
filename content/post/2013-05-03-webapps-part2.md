---
author: msaied93
categories:
- مقالات
date: 2013-05-03T06:33:14Z
guid: http://barmagiyathurra.wordpress.com/?p=25
id: 25
tags:
- Firefox OS
- Web Apps
- Web Design and Development
- العدد الثاني
- تطبيقات الوب
title: تصميم تطبيقات الوب، الجزء الثاني
featured_image: "uploads/2013/firefoxos_logo.png"
---

حاول الالتزام بالمبادئ التالية في تصميم التطبيق لتحسين تجربة المستخدم. لاحظ أن هذه مجرد مجموعة صغيرة من مبادئ التصميم، وهي مناسبة للانطلاق في تصميم التطبيق لكنها لا تشمل كل شيء.

<h2 style="color:#850505;">
  حافظ على الاتساق
</h2>

حافظ على الاتساق في كلماتك. استخدم دائماً مصطلحات موحدة للإشارة إلى العناصر أو الأفعال في التطبيق، واستخدم كل مصطلح لعنصر أو فعل واحد فقط.
  
حافظ على الاتساق في واجهة المستخدم (UI). فمثلاً، إذا وضعتَ زر الرجوع في الزاوية اليسرى العليا، فلا يجب أن يغير مكانه إلى الزاوية اليسرى السفلى في الشاشات الأخرى للتطبيق.
  
حافظ على الاتساق في العناصر الرسومية. فإذا كان مظهر أحد العناصر يطابق مظهر عنصر آخر، فيجب أن يكون لهما الوظيفة نفسها. وإذا كان للعنصرين وظيفتين مختلفتين، فيجب أن يكون مظهر كل منهما مختلفٌ عن الآخر.

<h2 style="color:#850505;">
  كن دقيقاً واستخدم كلمات ملائمة
</h2>

يجب أن يعبر النص عن أهم المعلومات بشكل مختصر. لأن معظم المستخدمين لا يقرؤون كثيراً قبل أن يقرروا المتابعة في المهمة أو إلغائها.

<h2 style="color:#850505;">
  احرص على عرض الدلائل والتغذية الراجعة
</h2>

أعط مستخدميك أدلة تجعل خطواتهم واضحة. على سبيل المثال، استخدم زر ”+“ كبير ليشير إلى عملية إضافة عناصر إذا كانت هذه الخطوة التالية المناسبة.
  
احرص دائماً على تقديم تغذية راجعة للمستخدمين عندما يتخذون قراراً. فمثلاً، عندما ينقر المستخدم على زر ”رسالة جديدة“، انقله مباشرة إلى شاشة إنشاء رسالة جديدة. يمكنك أيضاً تغيير لون زر ”رسالة جديدة“ مؤقتاً وإظهار أثر اللمسة حتى تبين أن تطبيقك يفهم أمر المستخدم.
  
إذا كان الأمر يستغرق أكثر من بضعة ثوان، فأظهر معلومات عن الحالة. مثلاً، إذا كان التطبيق ينزل الرسائل الجديدة، يمكنك عرض مستوى التقدم مصحوباً بأيقونة تحميل.

<h2 style="color:#850505;">
  اختصر الكتابة
</h2>

استخدم عناصر التحديد، أو صناديق التأشير، أو أزرار الاختيار الدائرية الخ. ، أي شيء آخر يمكنك استخدامه لتوفير الكتابة على المستخدم. الكتابة باستخدام الهواتف النقالة بطيئة ومملة أغلب الأحيان.

<h2 style="color:#850505;">
  اسع للوصول إلى تجربة ممتعة عند تشغيل التطبيق أول مرة
</h2>

غالباً ما يقرر الناس خلال الدقيقة الأولى أو الثانية من استخدام التطبيق فيما إذا كانوا سيستمرون في استخدامه أو سيهجرونه. نذكر لك بعضاً من العوامل التي تحتاج أخذها بعين الاعتبار:
  
وفر مقدمة سريعة تشرح التطبيق (ووفر طريقة سريعة لإنهائها في أي لحظة)
  
الوصول المباشر إلى المزايا والاقتراحات
  
قلل من طلب المدخلات من المستخدم
  
عبر عن أهم المعلومات بشكل مختصر

<h2 style="color:#850505;">
  اعرض خيارات فعلية إذا كان ذلك ممكناً
</h2>

إذا كان رسالة التحذير تقول ”هل تريد حذف جميع جهات الاتصال؟“ فيجب أن تضع كلمة ”حذف“ بدلاً من ”موافق“ لتنفيذ الفعل.
  
كما عليك أيضاً تفادي رسائل التحذير التي تنبه المستخدم إلى حالة ما فقط. بل أعط المستخدم عدة خيارات من الأفعال حتى يتعامل مع الحالة.

<h2 style="color:#850505;">
  ركز على المهمة الأساسية
</h2>

حدد المهمة الأساسية لكل شاشة وضع المزايا المتعلقة بتلك المهمة في أهم المناطق البارزة.

<h2 style="color:#850505;">
  توقع الانقطاعات
</h2>

قد يستخدم الناس تطبيقك في أي مكان. فضع في عين الاعتبار الانقطاعات وذلك من خلال حفظ عمل المستخدم في كل مرحلة أوتوماتيكياً إذا أمكن. في الحالة المثالية، يجب أن تعمل المهمات وتظهر المعلومات بشكل طبيعي حتى دون الاتصال بالإنترنت.

### المصدر:

<https://marketplace.firefox.com/developers/‎>

### ترخيص:

[CC BY-SA 3.0](http://creativecommons.org/licenses/by-sa/3.0/)
