---
author: msaied93
categories:
- مقالات
date: 2016-05-08T16:32:56Z
geo_public:
- 0
guid: https://barmagiyathurra.wordpress.com/?p=64
id: 64
tags:
- backports
- debian
- دبيان
- مستودعات
- منقولات خلفية
title: المنقولات الخلفية في دبيان
featured_image: "uploads/2016/05/debian-8-backports.png"
---

لنقل أنك تستخدم دبيان المستقرة، وتريد نسخة أحدث من برنامج معين بذاته، فكيف تحصل عليها؟ يمكنك إضافة المستودعات الاختبارية أو حتى غير المستقرة وتثبيت البرنامج المنشود، ولكن ماذا لو كان له اعتماديات كثيرة؟ ستصبح حزم كثيرة في نظامك غير مستقرة! هناك حل آخر وهو استخدام المنقولات الخلفية أو backports.

الفكرة هي إعادة ترجمة النسخ الحديثة من البرامج من المصدر ولكن استناداً على المكتبات والاعتماديات المتاحة في دبيان المستقرة، وبذلك تنتج نسخة حديثة من البرنامج مع اعتماديات يمكن تلبيتها ضمن دبيان المستقرة.

> العملية كما ترى تهدف إلى &#8220;نقل البرنامج إلى الوراء&#8221; أو &#8220;نقله خلفاً&#8221; وهكذا اخترت استخدام &#8220;المنقولات الخلفية&#8221; كمقابل لكلمة backports.

المنقولات الخلفية تؤخذ من دبيان الاختبارية (testing) وتعاد ترجمتها للعمل على دبيان المستقرة. وفي بعض الحالات القليلة تؤخذ الحزم من دبيان غير المستقرة.

الحزم المنقولة إلى دبيان المستقرة لا تختبر بشكل كاف، كما أنه لا توجد اي ضمانات بخصوص أدائها أو عدم تعارضها مع بقية مكونات دبيان المستقرة. هذا يعني أن تحاول تفادي استخدام الحزم المنقولة خلفاً قدر المستطاع والاقتصار على حزم قليلة فقط حسب حاجتك لها تفادياً لأي مشاكل قد تظهر فيها.

بالنسبة للتوزيعة المستقرة القديمة (دبيان ويزي في الوقت الحاضر)، فالمنقولات الخلفية تؤخذ من المستقرة الحالية (جيسي) وليس من الاختبارية الحالية (ستريتش)، أي أن المنقولات الخلفية تؤخذ من الإصدار التالي من دبيان. هذه المنقولات متاحة ضمن مستودع `wheezy-backports`.

إذاً، منقولات ويزي تأتي من دبيان جيسي ومنقولات جيسي تأتي من ستريتش (دبيان 9 &#8211; الاختبارية في الوقت الحاضر). لا يسمح للمنقولات من دبيان ستريتش بالرجوع إلى دبيان ويزي عبر مستودع wheezy-backports لأن هذا قد يعرقل عملية ترقية النظام إلى دبيان جيسي.

ورغم ذلك هناك مستودع ثان هو `wheezy-backports-sloppy` تؤخذ الحزم فيه من دبيان الاختبارية الحالية (دبيان ستريتش) وتنقل إلى المستقرة القديمة (ويزي)، بهدف توفير نسخ أحدث من البرمجيات والتطبيقات على دبيان ويزي.

## استخدام مستودع المنقولات الخلفية

بالنسبة دبيان جيسي أضف السطر التالي لملف `etc/apt/sources.list/`:

    deb http://httpredir.debian.org/debian jessie-backports main
    

أما في دبيان ويزي فعليك إضافة:

    deb http://httpredir.debian.org/debian wheezy-backports main
    

وبعد ذلك نفذ الأمر `apt-get update`.

لتثبيت حزمة ما من مستودع المنقولات الخلفية على دبيان جيسي استخدم الأمر التالي:

    apt-get -t jessie-backports install package-name
    

مع استبدال `package-name` باسم الحزمة التي تريد تثبيتها.

لاستخدام مستودع `wheezy-backports-sloppy` في ويزي عليك إضافة المستودع:

    deb http://httpredir.debian.org/debian wheezy-backports-sloppy main
    

ثم استخدام الأمر `apt-get install -t wheezy-backports-sloppy package-name` لتثبيت الحزم المطلوبة (بعد تحديث قوائم الحزم عبر `apt-get update` طبعاً).

## الحزم المتوفرة

الحزم سعيدة الحظ التي تتوفر لها منقولات خلفية قليلة، يمكنك الاطلاع على قائمة الحزم المتوفرة في مستودع `jessie-backports` مع أرقام إصداراتها عبر الرابط التالي: http://backports.debian.org/jessie-backports/overview

أما بالنسبة لمستودعي wheezy-backports وwheezy-backports-sloppy فمن الرابطين التاليين:
  
http://backports.debian.org/wheezy-backports/overview
  
http://backports.debian.org/wheezy-backports-sloppy/overview

## العلل

نظام تتبع علل دبيان Debian BTS لا يتابع العلل في المنقولات الخلفية، ولذلك إذا واجهتك علة في برمجية ثبتها من مستودعات المنقولات الخلفية فعليك التبليغ عنها على القائمة البريدية http://lists.debian.org/debian-backports وليس على Debian BTS.
