---
author: msaied93
categories:
- أخبار
date: 2013-05-02T21:42:31Z
geo_public:
- 0
guid: http://barmagiyathurra.wordpress.com/?p=12
id: 12
tags:
- Dropbox
- Scribd
- العدد الثاني
- روابط تنزيل
title: العدد الثاني من مجلة البرمجيات الحرة
featured_image: "uploads/2013/issue2-cover.jpg"
---

<p dir="RTL" align="RIGHT">
  روابط التنزيل:
</p>

<p dir="RTL" align="RIGHT">
  <a href="https://www.dropbox.com/s/rnwkzro6rfujtli/BH_magazine2.pdf?m">https://www.dropbox.com/s/rnwkzro6rfujtli/BH_magazine2.pdf?m</a>
</p>

<p dir="RTL" align="RIGHT">
  رابط Scribd
</p>

<p dir="RTL" align="RIGHT">
  <a href="http://bit.ly/11POWF4">http://bit.ly/11POWF4</a>
</p>
