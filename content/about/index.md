---
author: msaied93
date: 2013-05-02T01:38:03Z
title: عن مجلة البرمجيات الحرة
---

مجلة عربية تهتم بكل ما يتعلق بالبرمجيات الحرة والتقنيات مفتوحة المصدر. يكتبها لكم [msaied93](https://msaied93.github.io).

<div style="float:left" class="fb-page"
  data-href="https://www.facebook.com/barmagiyathurra"
  data-width="350"
  tabs="timeline,messages"></div>
